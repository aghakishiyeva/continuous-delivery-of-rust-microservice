# Stage 1: Build the Rust application
FROM rust:1.58 as builder
WORKDIR /usr/src/myapp
COPY . .
RUN cargo build --release

# Stage 2: Create the runtime environment
FROM debian:buster-slim
# Copy the built executable from the builder stage
COPY --from=builder /usr/src/myapp/target/release/individualproject2 /usr/local/bin/myapp
# Set the command to run your binary
CMD ["myapp"]
