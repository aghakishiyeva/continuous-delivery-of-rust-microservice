# Task Management Microservice

This project implements a task management microservice in Rust, featuring RESTful API endpoints for retrieving a list of tasks. It is containerized using Docker for easy deployment and scalability. Additionally, the project is integrated with a GitLab CI/CD pipeline for automated building and testing.



## Project Setup

### Requirements

- Rust and Cargo: Install via rustup by following the instructions on [Rust's official site](https://www.rust-lang.org/tools/install).
- Docker: For containerization. Download and install from [Docker's website](https://www.docker.com/products/docker-desktop).

### Creating the Project

- Initialize a new Rust project with Cargo: `cargo new task_management_microservice`.

## Development

The microservice uses Warp to serve RESTful API endpoints. The primary endpoint `/tasks` returns a JSON list of tasks, showcasing basic CRUD operations.

### Building and Running Locally

1. **Start the Rust Microservice**:
    - Navigate to your project directory.
    - Run `cargo run` to start the server.
    - The service will be accessible at `http://localhost:3030/tasks`.

## Dockerization

The provided `Dockerfile` prepares a Docker image of the microservice, streamlining deployment and ensuring the service runs consistently across different environments.

### Building and Running the Docker Container

- **Build the Docker Image**: `docker build -t task_management_microservice .`
- **Run the Container**: `docker run -p 3030:3030 task_management_microservice`

## Continuous Integration and Deployment

The `.gitlab-ci.yml` file at the root of the project configures the CI/CD pipeline in GitLab, automating the build and test processes for each code push.

### Pipeline Configuration

- **Build Stage**: Compiles the Rust project and verifies the build's integrity.
- The CI/CD pipeline ensures that the microservice is always in a deployable state, facilitating continuous delivery and integration practices.
