use serde::{Serialize};
use warp::Filter;

#[derive(Serialize)]
struct Task {
    id: u64,
    description: String,
}

async fn list_tasks() -> Result<impl warp::Reply, warp::Rejection> {
    let tasks = vec![
        Task { id: 1, description: "Learn Rust".to_string() },
        Task { id: 2, description: "Build a microservice".to_string() },
    ];

    Ok(warp::reply::json(&tasks))
}

#[tokio::main]
async fn main() {
    let list = warp::path!("tasks")
        .and(warp::get())
        .and_then(list_tasks);

    warp::serve(list)
        .run(([127, 0, 0, 1], 3030))
        .await;
}
